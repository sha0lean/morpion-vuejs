## Requirements

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/) >= 12.16.1
- yarn >= 1.22.0
- [Visual Studio Code](https://code.visualstudio.com/) with the [Vetur extension](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- Recent version of Google Chrome with the [Vue.js devtools extension](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

## Install

```bash
yarn install
```

## Usage

```bash
yarn serve
```


